package main

import (
	"beef-count/beef"
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {
	e := echo.New()
	e.GET("/beef/summary", func(c echo.Context) error {
		d := beef.CountBeef()
		return c.JSON(http.StatusOK, map[string]interface{}{
			"Beef": d,
		})
	})
	e.Use(middleware.Logger())

	e.Logger.Fatal(e.Start(":8080"))
}
