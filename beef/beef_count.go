package beef

import (
	"bytes"
	"io"
	"log"
	"net/http"
	"strings"
)

// removeSpecialCharacters -
func removeSpecialCharacters(s string) []string {
	replacer := strings.NewReplacer(",", "", ".", "", "\n", " ")
	s = replacer.Replace(s)
	return strings.Split(s, " ")
}

// GetDataBeef -
func GetDataBeef() string {
	resp, err := http.Get("https://baconipsum.com/api/?type=meat-and-filler&paras=99&format=text")
	if err != nil {
		log.Fatalln(err)
	}

	var bodyBytes bytes.Buffer
	_, err = io.Copy(&bodyBytes, resp.Body)

	if err != nil {
		log.Fatalln(err)
	}

	return string(bodyBytes.Bytes())
}

// CountBeef -
func CountBeef() map[string]int {
	text := GetDataBeef()

	textArr := removeSpecialCharacters(text)
	output := make(map[string]int)
	for _, t := range textArr {
		if t == "" {
			continue
		}
		key := strings.ToLower(t)
		if _, ok := output[key]; ok {
			output[key]++
			continue
		}
		output[key] = 1
	}
	return output
}
